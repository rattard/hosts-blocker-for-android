package com.ryanattard.websiteblocker;

import java.io.*;
import java.net.*;
import java.util.*;

import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;

import com.ryanttard.websiteblocker.*;

public class AddandStart extends ListActivity {
    ArrayAdapter<String> adapter;
    ArrayList<String> blockedhosts = new ArrayList<String>();
    // hostsfilename is destination hosts file
    String hostsfilename = "/etc/hosts";
    // backup is the location of the previously saved hosts file only done
    // once.
    String backupfilename = "hosts.bak";
    // savefilename is the previous iteration of the hosts file used for
    // reloading on resume
    String savefilename = "hosts.old";
    // newfilename is the new added hostsfile
    String newfilename = "hosts.new";
    // need a comment block for the hosts file to identify our part
    String onlyhostname = "onlyhostname";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_addand_start);
    }

    protected void onResume() {
	super.onResume();
    }

    protected void onStart() {
	super.onStart();
	populateBlockedHosts();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(R.menu.activity_addand_start, menu);
	return true;
    }

    public void addHost(View view) {
	EditText field = (EditText) findViewById(R.id.editHost);
	Object[] blockedhostarray = getResources().getStringArray(
		R.array.blocked_hosts);
	for (int i = 0; i < blockedhostarray.length; i++) {
	    blockedhosts.add(blockedhostarray[i].toString());
	}
	if (isValidNewHost(field.getText().toString())) {
	    blockedhosts.add(field.getText().toString());
	    blockedhostarray = blockedhosts.toArray();
	    field.setText("");
	    field.setHint("Added host!");
	} else {
	    field.setText("");
	    field.setHint("Not a valid host!");
	}
	if (!saveHost()) {
	    field.setText("");
	    field.setHint("Saving to hostfile failed");
	}
	adapter = new ArrayAdapter<String>(this,
		android.R.layout.simple_list_item_1, blockedhosts);
	setListAdapter(adapter);
	return;
    }

    public void removeHost(View view) {
	EditText field = (EditText) findViewById(R.id.remHost);
	String fieldtext = field.getText().toString();
	Object[] blockedhostarray = getResources().getStringArray(
		R.array.blocked_hosts);
	for (int i = 0; i < blockedhostarray.length; i++) {
	    blockedhosts.add(blockedhostarray[i].toString());
	}
	if (isValidExistingHost(fieldtext)) {
	    blockedhosts.remove(field.getText().toString());
	    field.setText("");
	    field.setHint("Removed host!");
	} else {
	    field.setText("");
	    field.setHint("Host not found!");
	}

	blockedhostarray = blockedhosts.toArray();
	adapter = new ArrayAdapter<String>(this,
		android.R.layout.simple_list_item_1, blockedhosts);
	setListAdapter(adapter);
	saveHost();
	return;
    }

    public void populateBlockedHosts() {
	android.util.Log.w("Made it to pop", "Populating!");

	String path = getFilesDir() + "siteblocker";

	try {
	    FileReader onlyhostfile = new FileReader(path + "/" + onlyhostname);
	    BufferedReader instream = null;
	    instream = new BufferedReader(onlyhostfile);
	    String hostline;
	    while ((hostline = instream.readLine()) != null
		    && isValidNewHost(hostline)) {
		blockedhosts.add(hostline);
		android.util.Log.w("foundhost", hostline);
	    }
	} catch (FileNotFoundException e) {
	    android.util.Log.w("noPreviousListFound",
		    "No previous list was found saved");
	    e.printStackTrace();
	    return;
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	adapter = new ArrayAdapter<String>(this,
		android.R.layout.simple_list_item_1, blockedhosts);
	setListAdapter(adapter);
	android.util.Log.w("donepopulate", blockedhosts.toString());

	return;
    }

    private boolean saveHost() {
	// To avoid supressing warnings, manually copy
	ArrayList<String> iterator = new ArrayList<String>();
	for (int i = 0; i < blockedhosts.size(); i++) {
	    iterator.add(blockedhosts.get(i));
	}
	String path = getFilesDir() + "siteblocker";
	String commentstart = "# Website Block Start";
	String commentend = "# Website Block End";
	String blockstring = "0.0.0.0 ";
	File hostsfile = new File(hostsfilename);
	File backupfile = new File(path, backupfilename);
	File pathfile = new File(path);
	File outfile = new File(path, newfilename);
	File savefile = new File(path, savefilename);
	File onlyhostfile = new File(path, onlyhostname);
	FileInputStream backupinstream = null;
	FileOutputStream backupoutstream = null;
	FileInputStream instream = null;
	FileOutputStream onlyhostoutstream = null;
	FileOutputStream outstream = null;

	if (backupfile.exists()) {
	    try {
		instream = new FileInputStream(backupfile);
	    } catch (FileNotFoundException e) {
		blockedhosts.clear();
		blockedhosts.add(e.toString() + "\n Instream creation Failure");
		return false;

	    }

	} else {
	    // First run only.
	    if (!pathfile.mkdirs()) {
		// if you can't make the directory for backup files
		if (!pathfile.isDirectory()) {
		    blockedhosts.clear();
		    blockedhosts.add("\n  Directory creation Failure for : "
			    + path);
		}
		return false;
	    }
	    try {
		backupfile.createNewFile();
	    } catch (IOException e) {
		blockedhosts.clear();
		blockedhosts.add(e.toString()
			+ "\n Backupfile creation Failure");
		return false;
	    }
	    byte[] buf = new byte[1024];
	    try {
		backupinstream = new FileInputStream(hostsfile);
		backupoutstream = new FileOutputStream(backupfile);

		while (backupinstream.read(buf) != -1) {
		    backupoutstream.write(buf);
		}
	    } catch (IOException e1) {
		// TODO Auto-generated catch block
		blockedhosts.clear();
		blockedhosts.add(e1.toString()
			+ "\n Backupfile creation Failure");
		return false;
	    }
	    // This is pretty critical. You have a backup which is important.
	    backupfile.setReadOnly();

	    try {
		instream = new FileInputStream(backupfile);
	    } catch (FileNotFoundException e) {
		blockedhosts.clear();
		blockedhosts.add(e.toString()
			+ "\n FileInputStream creation Failure");
		return false;

	    }
	}

	try {
	    outstream = new FileOutputStream(outfile, false);
	    onlyhostoutstream = new FileOutputStream(onlyhostfile, false);

	} catch (FileNotFoundException e) {

	    try {
		outfile.createNewFile();
		onlyhostfile.createNewFile();
	    } catch (IOException e1) {
		blockedhosts.clear();
		blockedhosts.add(e.toString() + "\n Outfile creation Failure");
		return false;
	    }

	}
	PrintStream printtonewhostsfile = new PrintStream(outstream);
	PrintStream printtoonlyhostsfile = new PrintStream(onlyhostoutstream);

	byte[] buf = new byte[1024];
	try {
	    while (instream.read(buf, 0, 1024) != -1) {
		try {
		    outstream.write(buf);
		    onlyhostoutstream.write(buf, 0, 0);
		} catch (IOException e1) {
		    blockedhosts.clear();
		    blockedhosts.add(e1.toString()
			    + "\n Outstream write  Failure");
		    return false;
		}
	    }
	} catch (IOException e) {
	    blockedhosts.clear();
	    blockedhosts.add(e.toString() + "\n Instream read Failure");
	    return false;
	}

	// This is just a simple short list.
	ArrayList<String> prefixes = new ArrayList<String>();
	prefixes.add("");
	prefixes.add("m.");
	prefixes.add("mail.");
	prefixes.add("mobile.");
	prefixes.add("mm.");
	prefixes.add("xhtml.");

	int lengthOfPrefixesNoWWW = prefixes.size();
	// To prevent infinite loop, you need to take the number of elements
	// before you start adding to them.

	for (int i = 0; i < lengthOfPrefixesNoWWW; i++) {
	    prefixes.add("www." + prefixes.get(i));
	}
	prefixes.add("www.");
	prefixes.add("www.");

	printtonewhostsfile.println(commentstart);

	while (!iterator.isEmpty()) {
	    String host = iterator.remove(0).toString();
	    printtoonlyhostsfile.println(host);
	    String line = blockstring + host;
	    for (int i = 0; i < prefixes.size(); i++) {
		printtonewhostsfile.println(line);
		line = blockstring + prefixes.get(i) + host;

	    }

	}
	printtonewhostsfile.println(commentend);
	try {
	    instream.close();
	    outstream.close();
	    onlyhostoutstream.close();
	} catch (IOException e) {
	    blockedhosts.clear();
	    blockedhosts.add(e.toString() + "\n File close Failure");
	}
	printtonewhostsfile.close();
	printtoonlyhostsfile.close();
	try {
	    java.lang.Process p = Runtime.getRuntime().exec("su");
	    String fullpathtonewfile = outfile.getAbsolutePath();
	    String fullpathtosavefile = savefile.getAbsolutePath();
	    String fullpathtobackupfile = backupfile.getAbsolutePath();
	    p.getOutputStream().write(
		    "mount -o rw,remount -t yaffs2 /dev/block/mtdblock3 /system \n"
			    .getBytes());
	    p.getOutputStream().write("cd /system/etc/ \n".getBytes());
	    p.getOutputStream().write("chmod 777 hosts \n".getBytes());

	    // Clear out comment block if no hosts remaining.
	    if (blockedhosts.isEmpty()) {
		p.getOutputStream().write(
			("cat " + fullpathtobackupfile + "> hosts \n")
				.getBytes());

	    } else {
		p.getOutputStream().write(
			("cat " + fullpathtonewfile + "> hosts \n").getBytes());

	    }

	    p.getOutputStream().write(
		    ("cat " + fullpathtonewfile + "> hosts \n").getBytes());
	    p.getOutputStream()
		    .write(("cat " + fullpathtonewfile + "> "
			    + fullpathtosavefile + "\n").getBytes());
	    p.getOutputStream().write("chmod 755 hosts \n".getBytes());

	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	    return false;
	}
	return true;

    }

    private Boolean isValidNewHost(String hostname) {
	if (blockedhosts.contains(hostname)) {
	    return false;
	}

	return isValidURI(hostname);
    }

    private Boolean isValidExistingHost(String hostname) {
	if (blockedhosts.contains(hostname)) {
	    return true;
	}
	return false;
    }

    private Boolean isValidURI(String hostname) {
	String URICharList = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~:/?#[]@!$&'()*+,;=";
	URL url;
	try {
	    url = new URL(hostname);
	} catch (MalformedURLException e) {
	    try {
		url = new URL("http://" + hostname);
	    } catch (MalformedURLException e1) {
		// TODO Auto-generated catch block
		return false;
	    }
	}
	return true;
    }
}
